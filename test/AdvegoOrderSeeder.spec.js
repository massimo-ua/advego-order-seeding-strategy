const AdvegoOrderSeeder = require('../lib/AdvegoOrderSeeder');
const {
    AddOrderError,
    AddThemesError,
    StartOrderError,
    StopOrderError,
} = require('../lib/errors');

const order = { title: 'test', themes_active: false };

describe('AdvegoOrderSeeder', () => {
    let seeder;
    let clientMock;

    beforeEach(() => {
        clientMock = {
            stopOrder: jest.fn().mockImplementation(() => Promise.resolve({})),
            addOrder: jest.fn().mockImplementation(() => Promise.resolve({ id_order: 1 })),
            editOrderThemes: jest.fn().mockImplementation(() => Promise.resolve({ themes: [] })),
            startOrder: jest.fn().mockImplementation(() => Promise.resolve({})),
        };
        seeder = new AdvegoOrderSeeder(clientMock);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should create AdvegoOrderSeeder instance', () => {
        expect(seeder).toBeDefined();
    });

    it('should call addOrder with profileId and order passed on send', async () => {
        await seeder.send(0, order);
        expect(clientMock.addOrder).toHaveBeenCalledWith(0, order);
    });

    it('should throw AddOrderError if addOrder request fails on send', done => {
        clientMock.addOrder.mockImplementation(() => Promise.resolve({ errors: [] }));
        seeder.send(0, order)
            .catch(error => {
                expect(error).toBeInstanceOf(AddOrderError);
                done();
            });
    });

    it('should call editOrderThemes with profileId and id_order and themes passed if themes_active option is true on send', async () => {
        const post_order = { ...order, themes_active: true, themes: []};
        await seeder.send(0, post_order);
        expect(clientMock.editOrderThemes).toHaveBeenCalledWith(0, { id_order: 1, themes: [] });
    });

    it('should throw AddThemesError if editOrderThemes request fails on send', done => {
        const post_order = { ...order, themes_active: true, themes: []};
        clientMock.editOrderThemes.mockImplementation(() => Promise.resolve({ error_msg: 'test' }));
        seeder.send(0, post_order)
            .catch(error => {
                expect(error).toBeInstanceOf(AddThemesError);
                done();
            });
    });

    it('should call startOrder with profileId and order id passed on send', async () => {
        await seeder.send(0, order);
        expect(clientMock.startOrder).toHaveBeenCalledWith(0, { ID: 1 });
    });

    it('should throw StartOrderError if startOrder request fails on send', done => {
        clientMock.startOrder.mockImplementation(() => Promise.resolve({ error_msg: 'test' }));
        seeder.send(0, order)
            .catch(error => {
                expect(error).toBeInstanceOf(StartOrderError);
                done();
            });
    });

    it('should call stopOrder with profileId and order id passed on stop', async () => {
        await seeder.stop(0, 1);
        expect(clientMock.stopOrder).toHaveBeenCalledWith(0, { ID: 1 });
    });

    it('should throw StopOrderError if stopOrder request fails on stop', done => {
        clientMock.stopOrder.mockImplementation(() => Promise.resolve({ error_msg: 'test' }));
        seeder.stop()
            .catch(error => {
                expect(error).toBeInstanceOf(StopOrderError);
                done();
            });
    });

    it('should call startOrder with profileId and order id passed on resume', async () => {
        await seeder.resume(0, 1);
        expect(clientMock.startOrder).toHaveBeenCalledWith(0, { ID: 1 });
    });

    it('should throw StartOrderError if startOrder request fails on resume', done => {
        clientMock.startOrder.mockImplementation(() => Promise.resolve({ error_msg: 'test' }));
        seeder.resume()
            .catch(error => {
                expect(error).toBeInstanceOf(StartOrderError);
                done();
            });
    });
});