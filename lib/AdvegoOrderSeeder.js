const {
    serializeAddOrderResponse,
    serializeAddThemesResponse,
    serializeStartOrderResponse,
    serializeStopOrderResponse,
} = require('./Helper');

class AdvegoOrderSeeder {
    constructor(client) {
        this.client = client;
    }

    async send(profileId, order) {
        const { themes, ...body } = order;
        const id_order = await this._addOrder(profileId, body);
        if (body.themes_active) {
            await this._addThemes(profileId, { themes, id_order });
        }
        await this._startOrder(profileId, id_order);
        return id_order;
    }

    stop(profileId, id) {
        const promise = this.client.stopOrder(profileId, { ID: id });
        return serializeStopOrderResponse(promise);
    }

    resume(profileId, id) {
        return this._startOrder(profileId, id);
    }

    _addOrder(profileId, order) {
        const promise = this.client.addOrder(profileId, order);
        return serializeAddOrderResponse(promise);
    }

    _addThemes(profileId, payload) {
        const promise = this.client.editOrderThemes(
            profileId,
            payload,
        );
        return serializeAddThemesResponse(promise);
    }

    _startOrder(profileId, ID) {
        const promise = this.client.startOrder(
            profileId,
            { ID },
        );
        return serializeStartOrderResponse(promise);
    }
}

module.exports = AdvegoOrderSeeder;