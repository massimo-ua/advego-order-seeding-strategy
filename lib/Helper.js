const {
    AddOrderError,
    AddThemesError,
    StartOrderError,
    StopOrderError,
} = require('./errors');

const AdvegoOrderHelper = {
    async serializeAddOrderResponse(promise) {
        try {
            const response = await promise;
            const { id_order, errors } = response;
            if (id_order) {
                return id_order;
            }
            throw new AddOrderError(errors);
        } catch(errors) {
            throw new AddOrderError(errors);
        }
    },

    async serializeAddThemesResponse(promise) {
        try {
            const response = await promise;
            const { themes, error_msg } = response;
            if (themes) {
                return themes;
            }
            throw new AddThemesError([error_msg]);
        } catch(errors) {
            throw new AddThemesError(errors);
        }
    },

    async serializeStartOrderResponse(promise) {
        try {
            const response = await promise;
            const { error_msg } = response;
            if (error_msg) {
                throw new StartOrderError([error_msg]);
            }
        } catch(errors) {
            throw new StartOrderError(errors);
        }
    },

    async serializeStopOrderResponse(promise) {
        try {
            const response = await promise;
            const { error_msg } = response;
            if (error_msg) {
                throw new StopOrderError([error_msg]);
            }
        } catch(errors) {
            throw new StopOrderError(errors);
        }
    }
}

module.exports = AdvegoOrderHelper;