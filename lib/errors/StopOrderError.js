const SeedingError = require('./OrderSeedingError');

class StopOrderError extends SeedingError {
    constructor(errors) {
        super({
            message: 'STOP_ORDER_REQUEST_FAILED',
            errors,
        });
    }
}

module.exports = StopOrderError;