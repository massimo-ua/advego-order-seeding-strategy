const SeedingError = require('./OrderSeedingError');

class AddThemesError extends SeedingError {
    constructor(errors) {
        super({
            message: 'ADD_THEMES_REQUEST_FAILED',
            errors,
        });
    }
}

module.exports = AddThemesError;