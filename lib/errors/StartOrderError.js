const SeedingError = require('./OrderSeedingError');

class StartOrderError extends SeedingError {
    constructor(errors) {
        super({
            message: 'START_ORDER_REQUEST_FAILED',
            errors,
        });
    }
}

module.exports = StartOrderError;