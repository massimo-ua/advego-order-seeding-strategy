const SeedingError = require('./OrderSeedingError');

class AddOrderError extends SeedingError {
    constructor(errors) {
        super({
            message: 'ADD_ORDER_REQUEST_FAILED',
            errors,
        });
    }
}

module.exports = AddOrderError;